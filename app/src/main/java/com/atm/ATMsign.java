package com.atm;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;

import androidx.room.Room;

import com.atm.models.SignDatabase;
import com.atm.models.SignatureItem;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ATMsign extends Application {

    private static Context context;
    private static String sigDirectory;
    private static boolean justSignature = false; // detached sau attached
    private static boolean en = true;
    public static ArrayList<SignatureItem> signaturesList;
    //fisierul in care se scriu itemii din lista de semnaturi
    private static String serializedFile = "sinatures.dat";
    private static SignDatabase database;

    public void onCreate() {
        super.onCreate();
        ATMsign.context = getApplicationContext();
        int THREADPOOL_SIZE =4;
        ExecutorService databaseService = Executors.newFixedThreadPool(THREADPOOL_SIZE);
        database = Room.databaseBuilder(
                context,SignDatabase.class,"SignDatabase"
        ).allowMainThreadQueries().setQueryExecutor(databaseService).build();
        //Citim din fisierul de configurare directorul unde se salveaza semnaturile
        SharedPreferences sh = getSharedPreferences("sharedPref",MODE_PRIVATE);
        sigDirectory = sh.getString("sigDirectory", "-1");
        justSignature = sh.getBoolean("justSig",false);
        en = sh.getBoolean("language",true);
        if (sigDirectory.equals("-1")) {
            sigDirectory="storage/emulated/0/ATMSignature";

        }
        readSigList();
    }


    public static Context getAppContext() {
        return ATMsign.context;
    }

    public static void setSigDirectory(String newDirectory) throws IOException {
        sigDirectory = "storage/emulated/0/" + newDirectory;
        File file = new File(sigDirectory);
        if (!file.exists())
            file.mkdir();
    }
    public static void setSigMethod(Boolean _justSignature) throws IOException {
        justSignature = _justSignature;
    }
    public static void setEn(Boolean en1) {
        en = en1;
    }

    public static Boolean getEn() {
        return en;
    }

    public static Boolean getJustSig() {
        return justSignature;
    }
    public static String getSigDirectory() {
        return sigDirectory;
    }

    public static String getRelativeDir() {
        return sigDirectory.substring(19);
    }


    public static void saveConfig()
    {
        SharedPreferences sh = context.getSharedPreferences("sharedPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = sh.edit();
        editor.putString("sigDirectory",sigDirectory);
        editor.putBoolean("justSig",justSignature);
        editor.putBoolean("language",en);
        editor.commit();
    }

    /*
    Deserializeaza din fisier lista de semnaturi
     */
    public static void readSigList() {

        List<SignatureItem> list = database.signDAO().getSignatures();
        signaturesList = new ArrayList<>(list);
        for( SignatureItem signature : signaturesList){
            signature.setSaved(true);
        }
    }
    /*
    Se salveaza in baza de date semnaturile nesalvate
     */
    public static void writeSigList() {

        for (SignatureItem signatureItem : signaturesList){
            if (!signatureItem.isSaved())
                database.signDAO().insertSigItem(signatureItem);
            signatureItem.setSaved(true);
        }


    }


    public static void newSignatureItem(SignatureItem signatureItem) {
        signaturesList.add(signatureItem);
        writeSigList();
    }

    public static void deleteSignatureItem(int position) {
        signaturesList.remove(position);

    }
}
