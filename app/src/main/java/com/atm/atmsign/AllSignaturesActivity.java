package com.atm.atmsign;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.atm.ATMsign;
import com.atm.models.SignatureAdapter;

public class AllSignaturesActivity extends AppCompatActivity {

    private SignatureAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new SignatureAdapter(ATMsign.signaturesList, this);
        setContentView(R.layout.activity_all_signatures);

        ListView listView = (ListView) findViewById(R.id.signList);
        listView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.settingsOption:
                startActivity(new Intent(this,SettingsActivity.class));
                break;
            case R.id.signOption:
                startActivity(new Intent(this,SignActivity.class));
                break;
            case R.id.verifyOption:
                startActivity(new Intent(this,VerifyActivity.class));
                break;
            case R.id.signaturesOption:
                startActivity(new Intent(this,AllSignaturesActivity.class));
                break;
            case R.id.detailsOption:
                startActivity(new Intent(this,AboutActivity.class));
        }
        return true;
    }

    public void saveClicked(View view){

        ATMsign.writeSigList();
        this.finish();
    }
}
