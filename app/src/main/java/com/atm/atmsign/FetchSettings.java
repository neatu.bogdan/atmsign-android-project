package com.atm.atmsign;

import android.os.AsyncTask;
import android.util.JsonReader;

import com.atm.models.Setting;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FetchSettings extends AsyncTask <String, Integer,Setting> {


    @Override
    protected Setting doInBackground(String... strings) {
        try {
            URL url = new URL(strings[0]);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            StringBuilder data = new StringBuilder();
            while(line != null){
                data.append(line);
                line = bufferedReader.readLine();
            }
            JSONObject jsonObject = new JSONObject(data.toString());
            jsonObject = (JSONObject) jsonObject.get("settings");
            return new Setting(jsonObject.get("language").toString(),jsonObject.get("type").toString(),jsonObject.get("folder").toString()) ;

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
