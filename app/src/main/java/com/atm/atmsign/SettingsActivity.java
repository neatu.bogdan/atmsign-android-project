package com.atm.atmsign;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.atm.ATMsign;
import com.atm.models.Setting;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        RadioGroup radioGroup = findViewById(R.id.radioGroup) ;

        if(ATMsign.getJustSig())
            radioGroup.check(R.id.justSigRb);
        else
            radioGroup.check(R.id.withDocRb);
        EditText dirView = findViewById(R.id.directoryView);
        dirView.setText(ATMsign.getRelativeDir());

        Button saveBtn = findViewById(R.id.saveButton);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    saveConfig();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        RadioButton en = findViewById(R.id.enRb);
        if (ATMsign.getEn()==true)
            en.setChecked(true);
        else
        {
            en=findViewById(R.id.roRb);
            en.setChecked(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.settingsOption:
                startActivity(new Intent(this,SettingsActivity.class));
                break;
            case R.id.signOption:
                startActivity(new Intent(this,SignActivity.class));
                break;
            case R.id.verifyOption:
                startActivity(new Intent(this,VerifyActivity.class));
                break;
            case R.id.signaturesOption:
                startActivity(new Intent(this,AllSignaturesActivity.class));
                break;
            case R.id.detailsOption:
                startActivity(new Intent(this,AboutActivity.class));
        }
        return true;
    }

    public void fetchConfig (View view){

        FetchSettings fetchSettings = new FetchSettings();
        try {
            Setting setting= fetchSettings.execute("http://192.168.1.3").get();
            EditText folder = (EditText) findViewById(R.id.directoryView);
            folder.setText(setting.getFolder());
            RadioButton english = (RadioButton) findViewById(R.id.enRb);
            if(setting.getLanguage().equals("english"))
                english.setChecked(true);
            RadioButton attached = (RadioButton) findViewById(R.id.withDocRb);
            if(setting.getType().equals("attached"))
                attached.setChecked(true
                );
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }


    }

    private void saveConfig() throws IOException, InterruptedException {
        RadioButton justSig = findViewById(R.id.justSigRb);
        if (justSig.isChecked())
            ATMsign.setSigMethod(true);
        else
            ATMsign.setSigMethod(false);
        EditText directory = findViewById(R.id.directoryView);
        String dir = directory.getText().toString();
        if(dir!=null) ATMsign.setSigDirectory(dir);
        ProgressDialog progressDialog = new ProgressDialog(this) ;
        progressDialog.setMessage("Saving...");
        progressDialog.setCancelable(false);
        //progressDialog.setIndeterminate(false);
        progressDialog.show();
        Thread.sleep(100);
        progressDialog.dismiss();

        RadioButton en = findViewById(R.id.enRb);
        String lang ;
        if(en.isChecked())
        {
            ATMsign.setEn(true);
            lang="en";
        }else {
            ATMsign.setEn(false);
            lang = "ro";
        }
        Locale myLocale = new Locale(lang);
        Resources res = this.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        ATMsign.saveConfig();
    }
}
