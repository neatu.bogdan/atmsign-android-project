package com.atm.atmsign;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.media.tv.TvContract;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.security.KeyChain;
import android.security.KeyChainAliasCallback;
import android.security.KeyChainException;
import android.security.keystore.KeyProperties;
import android.text.style.TextAppearanceSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;

import com.atm.ATMsign;
import com.atm.crypto.EntityPrivateKey;
import com.atm.models.SignatureItem;
import com.atm.utils.FileUtils;

import org.w3c.dom.Text;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class SignActivity extends AppCompatActivity {

    private File myFile=null;
    private Uri fileUri =null;
    private X509Certificate certificate=null;
    private PrivateKey privKeyBind = null;
    private String filename=null;


    private EntityPrivateKey entityPrivateKey=null;
    private boolean emptyCert=true;
    private boolean emptyDoc=true;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.settingsOption:
                startActivity(new Intent(this,SettingsActivity.class));
                break;
            case R.id.signOption:
                startActivity(new Intent(this,SignActivity.class));
                break;
            case R.id.verifyOption:
                startActivity(new Intent(this,VerifyActivity.class));
                break;
            case R.id.signaturesOption:
                startActivity(new Intent(this,AllSignaturesActivity.class));
                break;
            case R.id.detailsOption:
                startActivity(new Intent(this,AboutActivity.class));
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        TextView tv = findViewById(R.id.certificateTw);
        tv.setTypeface(Typeface.DEFAULT);
        /*
        Apasarea butonului pentru alegerea documentului
         */
        ImageButton  browseFileBtn = findViewById(R.id.browseFileBtn);
        browseFileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                browseClicked();
            }
        });

        /*
        Apasarea butonului pentru selectarea certificatului
         */
        ImageButton certBtn = findViewById(R.id.browseCertBtn);
        certBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                certButtonClicked();
            }
        });

        /*
        Apasarea butonului Sign
         */
        Button signButton = findViewById(R.id.signButton);
        signButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    singButtonClicked();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    /*
    La intoarcerea din file manager
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RequestCodes.SELECT_FILE:
                if (resultCode == RESULT_OK) {
                    fileSelected(data);
                }
                break;

        }
    }

    /*
   Callback click buton de browse
    */
    private void browseClicked(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/*");
        startActivityForResult(intent, RequestCodes.SELECT_FILE);
    }

    /*
    Callback la apasarea butonului de selectare certificat
     */
    private void certButtonClicked(){
        try {
            KeyChain.choosePrivateKeyAlias(SignActivity.this, new KeyChainAliasCallback() {
                        public void alias(String alias) {
                            if (alias != null) {
                                certificateSelected(alias);
                            } else {
                                certificateClear();
                            }
                        }
                    },
                    new String[]{KeyProperties.KEY_ALGORITHM_RSA, "DSA"}, // List of acceptable key types. null for any
                    null,                        // issuer, null for any
                    null,                        // host name of server requesting the cert, null if unavailable
                    -1,                          // port of server requesting the cert, -1 if unavailable
                    "");                         // alias to preselect, null if unavailable


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    Se apeleaza la apasarea butonului pentru semnarea fisierului incarcat cu certificatul selectat
    Se verificat
     */
    private void singButtonClicked() throws InterruptedException {
        /*
        Se apasa sign inainte sa se selecteze un certificat
         */
        if(certificate==null)
        {
            alertCert(getResources().getString(R.string.certAlert));
            if(myFile == null)
            {
                alertFile(getResources().getString(R.string.fileAlert));
                return;
            }
            return;
        }
        if(myFile == null)
        {
            alertFile(getResources().getString(R.string.fileAlert));
            return;
        }

        ProgressDialog progressDialog = new ProgressDialog(this) ;
        progressDialog.setMessage("Signing...");
        progressDialog.setCancelable(false);
        //progressDialog.setIndeterminate(false);

        progressDialog.show();
        //identitatea selectata
        entityPrivateKey = new EntityPrivateKey(certificate,privKeyBind);
        SigningJob job= new SigningJob(this,entityPrivateKey,fileUri,progressDialog);
        job.execute();

    }

    /*
    Selectarea unui document. Se va apela cand utilizatorul a selectat un document pentru semnat
     */
    private void fileSelected(Intent data) {
        TextView textView = findViewById(R.id.fileToSignTw);
        fileUri= data.getData();
        String uriString = fileUri.toString();
        myFile=new File(uriString);

        textView.setBackgroundResource(R.drawable.textbox_background);
        textView.setTextColor(getResources().getColor(R.color.TextColor));
        filename = FileUtils.getFile(this,fileUri).getName();
        if(filename!=null)
        {
            //textView.setTypeface(null, Typeface.BOLD);
            textView.setBackgroundResource(R.drawable.textbox_layout_g);
            textView.setText(filename);
            textView.setTextColor(getResources().getColor(R.color.TextColor));
            emptyDoc=false;
            textView = findViewById(R.id.fileAlertTv);
            textView.setVisibility(View.INVISIBLE);
        }
        else
        {
            textView.setText(getResources().getString(R.string.file_to_sign_tv));
            textView.setBackgroundResource(R.drawable.textbox_background);
            textView.setTypeface(Typeface.DEFAULT);
            textView.setTextColor(getResources().getColor(R.color.gray));
            emptyDoc=true;
            textView = findViewById(R.id.fileAlertTv);
            textView.setVisibility(View.INVISIBLE);
        }
    }

    /*
    Selectarea unui certitifcate din store
    Se Afiseaza in textbox alias-ul certificatului si se retine certificatul si un 'pointer' a cheii private
     */
    private void certificateSelected(String alias) {
        TextView tv = findViewById(R.id.certificateTw) ;
        tv.setText(alias);
        tv.setBackgroundResource(R.drawable.textbox_layout_g);
        tv.setTextColor(getResources().getColor(R.color.TextColor));
        //tv.setTypeface(null, Typeface.BOLD);
        tv = findViewById(R.id.certAlertTv);
        tv.setVisibility(View.INVISIBLE);
        findViewById(R.id.KUAlert).setVisibility(View.INVISIBLE);
        emptyCert=false;
        try {
            certificate = KeyChain.getCertificateChain(SignActivity.this, alias)[0];
            privKeyBind = KeyChain.getPrivateKey(SignActivity.this,alias);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (KeyChainException e) {
            e.printStackTrace();
        }
    }

    private void alertCert (String alert) {
        TextView tv = findViewById(R.id.certificateTw);
        tv.setBackgroundResource(R.drawable.textbox_layout_r);
        tv.setTextColor(getResources().getColor(R.color.red));
        tv = findViewById(R.id.certAlertTv);
        tv.setText(alert);
        tv.setVisibility(View.VISIBLE);
    }

    private void alertFile (String alert) {
        TextView tv = findViewById(R.id.fileToSignTw);
        tv.setBackgroundResource(R.drawable.textbox_layout_r);
        tv.setTextColor(getResources().getColor(R.color.red));
        tv = findViewById(R.id.fileAlertTv);
        tv.setText(alert);
        tv.setVisibility(View.VISIBLE);
    }


    /*
    Se elimina din memorie certificatul si pointer-ul la cheia privata
    Se elibereaza campurile corespunzatoare si din interfata grafica
     */
    private void certificateClear() {
        TextView tv = findViewById(R.id.certificateTw) ;
        tv.setText(getResources().getString(R.string.your_certificate));
        tv.setTextColor(getResources().getColor(R.color.gray));
        tv.setBackgroundResource(R.drawable.textbox_background);
        tv.setTypeface(Typeface.DEFAULT);
        emptyCert=true;
        certificate=null;
        privKeyBind=null;
    }

    /*
    Terminarea task-ului care semneaza documentul
     */
    public void signFinished() {
        if (SigningJob.ok==true) {
            Intent intent =new Intent(this, SuccesActivity.class);
            intent.putExtra("SIGNED_FILENAME",filename);
            SignatureItem signatureItem = new SignatureItem(filename+".p7s",certificate.getSubjectDN().toString(),ATMsign.getSigDirectory()+"/"+filename+".p7s",ATMsign.getJustSig());
            if(ATMsign.signaturesList == null)
                ATMsign.signaturesList = new ArrayList<>();
            ATMsign.signaturesList.add(signatureItem);
            startActivity(intent);
            SigningJob.ok = false;
        }
        else
        {
           alertCert(getString(R.string.kualert));
            // findViewById(R.id.KUAlert).setVisibility(View.VISIBLE);
        }
    }

    public void writeSignedData(byte [] SignedData) throws IOException {
        String path = ATMsign.getSigDirectory()+"/" + filename+ ".p7s";
        BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(new File(path)));
        writer.write(SignedData,0,SignedData.length);
        int a =SignedData.length;
        writer.close();
    }
}

