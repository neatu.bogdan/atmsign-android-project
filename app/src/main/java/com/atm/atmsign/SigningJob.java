package com.atm.atmsign;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.atm.crypto.EntityPrivateKey;

import java.io.File;

public class SigningJob extends AsyncTask<Void,Void,Void> {
    private EntityPrivateKey entityPrivateKey;
    private Uri fileUri;
    private ProgressDialog progressDialog;
    private SignActivity parent;

    public static boolean ok = false ;

    SigningJob(SignActivity activity, EntityPrivateKey entity, Uri uri, ProgressDialog pd)
    {
        parent = activity;
        entityPrivateKey=entity;
        fileUri = uri;
        progressDialog = pd;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            /*
            Se semneaza documentul si se va scrie in fisier
             */
            parent.writeSignedData(entityPrivateKey.CMSSign(fileUri));
            ok=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        progressDialog.dismiss();
        parent.signFinished();
    }
}
