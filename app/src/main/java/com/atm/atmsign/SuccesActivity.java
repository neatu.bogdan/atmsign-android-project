package com.atm.atmsign;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.atm.ATMsign;
import com.atm.utils.FileUtils;

import java.io.File;
import java.net.URI;
import java.util.Objects;

public class SuccesActivity extends AppCompatActivity {

    private String filename = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_succes);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.settingsOption:
                startActivity(new Intent(this,SettingsActivity.class));
                break;
            case R.id.signOption:
                startActivity(new Intent(this,SignActivity.class));
                break;
            case R.id.verifyOption:
                startActivity(new Intent(this,VerifyActivity.class));
                break;
            case R.id.signaturesOption:
                startActivity(new Intent(this,AllSignaturesActivity.class));
                break;
            case R.id.detailsOption:
                startActivity(new Intent(this,AboutActivity.class));
        }
        return true;
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        TextView tv = findViewById(R.id.signedFileTv);
        filename = getIntent().getStringExtra("SIGNED_FILENAME");
        tv.setText(ATMsign.getRelativeDir()+"/"+filename+".p7s");

        Button shareBtn = findViewById(R.id.shareBtn);
        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareFile();
            }
        });

        Button homeButton = findViewById(R.id.homeButton);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnHome();
            }
        });
    }

    private void returnHome() {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }

    private void shareFile() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        final File file = new File(ATMsign.getSigDirectory()+"/"+filename+".p7s");
        Uri fileUri = null;

        fileUri=FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                BuildConfig.APPLICATION_ID + ".provider", file);;
        intent.putExtra(Intent.EXTRA_STREAM,fileUri);
        intent.setType("*/*");
        startActivity(Intent.createChooser(intent, "Share File"));
    }

}
