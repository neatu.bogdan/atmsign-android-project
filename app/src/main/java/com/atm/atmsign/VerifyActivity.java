package com.atm.atmsign;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import java.io.File;

public class VerifyActivity extends AppCompatActivity {
    private File contentFile = null;
    private File sigFile = null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        ImageButton chooseSig = findViewById(R.id.chSigBtn);
        chooseSig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseSignatureFile();
            }
        });

    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {

        super.onPostCreate(savedInstanceState);


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.settingsOption:
                startActivity(new Intent(this,SettingsActivity.class));
                break;
            case R.id.signOption:
                startActivity(new Intent(this,SignActivity.class));
                break;
            case R.id.verifyOption:
                startActivity(new Intent(this,VerifyActivity.class));
                break;
            case R.id.signaturesOption:
                startActivity(new Intent(this,AllSignaturesActivity.class));
                break;
            case R.id.detailsOption:
                startActivity(new Intent(this,AboutActivity.class));
        }
        return true;
    }


    private void chooseContentFile() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/*");
        startActivityForResult(intent, RequestCodes.SELECT_FILE);
    }

    private void chooseSignatureFile(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("application/*");
        startActivityForResult(intent, RequestCodes.SELECT_FILE);
    }
}
