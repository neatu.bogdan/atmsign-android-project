package com.atm.crypto;

import android.net.Uri;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import org.spongycastle.cert.jcajce.JcaCertStore;
import org.spongycastle.cms.CMSException;
import org.spongycastle.cms.CMSProcessableByteArray;
import org.spongycastle.cms.CMSSignedData;
import org.spongycastle.cms.CMSSignedDataGenerator;
import org.spongycastle.cms.CMSTypedData;
import org.spongycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.spongycastle.jce.provider.BouncyCastleProvider;
import org.spongycastle.operator.ContentSigner;
import org.spongycastle.operator.OperatorCreationException;
import org.spongycastle.operator.jcajce.JcaContentSignerBuilder;
import org.spongycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.spongycastle.util.Store;

import com.atm.ATMsign;
import com.atm.utils.FileUtils;

public class EntityPrivateKey {
    private PrivateKey privKey = null;
    private X509Certificate certificate = null;

    public EntityPrivateKey(X509Certificate cert, PrivateKey key) {
        privKey=key;
        certificate=cert;
    }

    public boolean verifyKeyUsage() throws Exception {
        boolean[] keyUsages =  certificate.getKeyUsage();
        if(!keyUsages[0] || !keyUsages[1])
            throw new Exception("Certificatul folosit nu este destinat semnarii de documente");
        return true;
    }

    /**
     * CMS Document signing
     * @param fileUri
     * @return The byte array that represents the CMS signature (ASN1 DER format)
     * @throws Exception
     */

    public byte[] CMSSign(Uri fileUri) throws Exception {
        String path = FileUtils.getPath(ATMsign.getAppContext(),fileUri);
        File file = new File(path);
        int len = (int) file.length();
        byte [] dataToSign = new byte[len];
        //citim datele fisierului care urmeaza sa fie semnat
        FileInputStream input = new FileInputStream(file);
        input.read(dataToSign,0,len) ;
        input.close();
        //Verific scopul certificatului
        verifyKeyUsage();
        Security.addProvider(new BouncyCastleProvider());
        byte[] signedData;
        List<X509Certificate> certList = new ArrayList<X509Certificate>();
        certList.add(certificate);
        CMSTypedData cmsData = new CMSProcessableByteArray(dataToSign);
        Store certs = new JcaCertStore(certList);

        CMSSignedDataGenerator cmsGenerator = new CMSSignedDataGenerator();
        ContentSigner contentSigner = new JcaContentSignerBuilder("SHA256WithRSA").build(privKey);
        cmsGenerator.addSignerInfoGenerator(new JcaSignerInfoGeneratorBuilder(
                new JcaDigestCalculatorProviderBuilder().setProvider("BC")
                .build()) .build(contentSigner, certificate));
        cmsGenerator.addCertificates(certs);
        CMSSignedData cms = cmsGenerator.generate(cmsData, !ATMsign.getJustSig());
        signedData = cms.getEncoded();
        return signedData;
    }

}
