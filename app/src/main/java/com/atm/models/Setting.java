package com.atm.models;

public class Setting {
    private String language;
    private String type ;

    public Setting(String language, String type, String folder) {
        this.language = language;
        this.type = type;
        this.folder = folder;
    }

    private String folder;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
