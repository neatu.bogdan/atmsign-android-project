package com.atm.models;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SignDAO {
    @Insert
    void insertSigItem(SignatureItem signatureItem);

    @Delete
    void deleteSigItem(SignatureItem signatureItem);

    @Query("SELECT * FROM signatures")
    List<SignatureItem> getSignatures();
}
