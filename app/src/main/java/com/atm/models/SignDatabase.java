package com.atm.models;


import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.Entity;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

@Database(entities = SignatureItem.class, version = 1)
public abstract class SignDatabase extends RoomDatabase {

    public abstract SignDAO signDAO();

}
