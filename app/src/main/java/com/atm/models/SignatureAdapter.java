package com.atm.models;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.atm.ATMsign;
import com.atm.atmsign.R;

import java.security.Signature;
import java.util.ArrayList;

public class SignatureAdapter extends BaseAdapter {
    private ArrayList<SignatureItem> signatures;
    private Context context;

    public SignatureAdapter(ArrayList<SignatureItem> signatures, Context context) {
        this.signatures = signatures;
        this.context = context;
    }

    public void addElement(SignatureItem signature) {
        this.signatures.add(signature);
        notifyDataSetChanged();
    }

    public void deleteElement(int position){
        this.signatures.remove(position);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return this.signatures.size();
    }

    @Override
    public Object getItem(int position) {
        return this.signatures.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View viewSigItem = inflater.inflate(R.layout.sginature_item,parent,false);
        TextView tvSignFileName = (TextView) viewSigItem.findViewById(R.id.tvSignFileName);
        TextView tvSignerName = (TextView) viewSigItem.findViewById(R.id.tvSigner);

        SignatureItem signatureItem = signatures.get(position);

        ImageView sigImg = (ImageView) viewSigItem.findViewById(R.id.sigImg);

        tvSignerName.setText(signatureItem.getSignerAlias());

        tvSignFileName.setText(signatureItem.getName());

        if(signatureItem.isAttached())
            sigImg.setImageResource(R.mipmap.attached);
        else
            sigImg.setImageResource(R.mipmap.detached);
        if (!signatureItem.isSaved())
            viewSigItem.setBackgroundColor(parent.getResources().getColor(R.color.gray));
        viewSigItem.setMinimumWidth(parent.getWidth());
        return viewSigItem;
    }
}
