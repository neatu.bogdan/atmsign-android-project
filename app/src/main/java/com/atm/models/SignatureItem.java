package com.atm.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
@Entity(tableName = "signatures")
public class SignatureItem implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "signerAlias")
    private String signerAlias;
    @ColumnInfo(name = "path")
    private String path;
    @ColumnInfo(name = "isAttached")
    private boolean isAttached;
    @ColumnInfo(name = "isSaved")
    private boolean isSaved = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public SignatureItem(String name, String signerAlias, String path, boolean isAttached) {
        this.name = name;
        this.signerAlias = signerAlias;
        this.path = path;
        this.isAttached = isAttached;
    }

    public String getName() {
        return name;
    }

    public String getSignerAlias() {
        return signerAlias;
    }

    public String getPath() {
        return path;
    }

    public boolean isAttached() {
        return isAttached;
    }

    public boolean isSaved(){
        return isSaved;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setSignerAlias(String signerAlias) {
        this.signerAlias = signerAlias;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setAttached(boolean attached) {
        isAttached = attached;
    }

    public void setSaved(boolean saved){
        isSaved = saved;
    }
}
